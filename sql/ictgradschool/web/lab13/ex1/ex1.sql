# Answers to exercise 1 questions
# A. Display the departments offering courses
SELECT DISTINCT dept
FROM unidb_courses;


# B. Display the semesters being attended
SELECT DISTINCT semester
FROM unidb_attend;

# C. Display the courses that are attended
SELECT DISTINCT dept, num
FROM unidb_attend;

# D. List the student names and country, ordered by first name
SELECT fname, lname, country
FROM unidb_students
ORDER BY fname;

# E. List the student names and mentors, ordered by mentors
SELECT fname, lname, mentor
FROM unidb_students
ORDER BY mentor;

# F. List the lecturers, ordered by office
SELECT fname, lname
FROM unidb_lecturers
ORDER BY office;

# G. List the staff whose staff number is greater than 500
SELECT fname, lname
FROM unidb_lecturers
WHERE staff_no > 500;

# H. List the students whose id is greater than 1668 and less than 1824
SELECT fname, lname
FROM unidb_students
WHERE  id > 1668 AND id < 1824;

# I. List the students from NZ, Australia and US
SELECT fname, lname
FROM unidb_students
WHERE country IN ('NZ', 'AU','US');

# J. List the lecturers in G Block
SELECT fname, lname
FROM unidb_lecturers
WHERE office LIKE 'G%';

# K. List the courses not from the Computer Science Department
SELECT num
FROM unidb_courses
WHERE dept NOT IN ('comp');

# L. List the students from France or Mexico
SELECT fname, lname
FROM unidb_students
WHERE country IN ('MX', 'FR');