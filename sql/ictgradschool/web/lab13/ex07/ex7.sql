DROP TABLE IF EXISTS isCaptain;
DROP TABLE IF EXISTS Apart_of;
DROP TABLE IF EXISTS LeagueOfSport;
DROP TABLE IF EXISTS Team;
DROP TABLE IF EXISTS player;


CREATE TABLE LeagueOfSport (
  team_ID int(20) PRIMARY KEY NOT NULL

);

CREATE TABLE Team (
  team_ID int(100) PRIMARY KEY NOT NULL,
  name varchar(32) NOT NULL,
  home_City char(50) NOT NULL NULL,
  number_of_points int(100),
  captain varchar(100) NOT NULL
);

CREATE TABLE player (
  player_ID int(100) NOT NULL,
  teamOf int(100) NOT NULL,
  name varchar(50) NOT NULL,
  age varchar(50) NOT NULL,
  nationality char(100) DEFAULT NULL,
  club char(100) REFERENCES Team(team_ID)
);



INSERT INTO Team (team_ID, name, home_City, number_of_points, captain) VALUES
  (1, 'goodteam', 'auckland', 20, 'john'),
  (2, 'badteam', 'Nelson', 5, 'Bob'),
  (3, 'averageTean', 'Christchurch', 10, 'Jason');

INSERT INTO player (player_ID, teamOf, name, age, club) VALUES
  (1, 1, 'john', 20, 1),
  (2, 2, 'Bob', 30, 2),
  (3, 3, 'Jason', 35, 3),
  (4, 3, 'Neil', 49, 4);

INSERT INTO LeagueOfSport (team_ID) VALUES
  (1),
  (2),
  (3);



SELECT team_ID, name, home_City, number_of_points, captain
FROM Team;

SELECT player_ID, teamOf, name, age
FROM player;

