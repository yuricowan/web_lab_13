# Answers to exercise 2 questions
-- What are the names of the students who attend COMP219
SELECT
  students.fname,
  students.lname
FROM unidb_attend AS attend, unidb_students AS students
WHERE attend.id = students.id AND attend.dept = 'comp' AND attend.num = '219';

-- What are the names of the student reps that are not from NZ?
SELECT
  students.fname,
  students.lname
FROM unidb_courses AS course, unidb_students AS students
WHERE course.rep_id = students.mentor AND students.country != 'NZ';

-- Where are the offices for the lecturers of 219
SELECT lecturer.office
FROM unidb_teach AS teach, unidb_lecturers AS lecturer
WHERE lecturer.staff_no = teach.staff_no AND teach.num = '219';

-- What are the names of the students taught by Te Taka
SELECT DISTINCT
  students.fname,
  students.lname,
  students.id,
  lecturer.fname,
  lecturer.staff_no
FROM unidb_lecturers AS lecturer, unidb_students AS students, unidb_teach AS teach, unidb_attend AS attend
WHERE lecturer.staff_no = teach.staff_no AND lecturer.staff_no = '707' AND students.id = attend.id;

-- List the students and their mentors
SELECT
  student.fname,
  student.lname,
  lecturer.fname,
  lecturer.lname
FROM unidb_courses AS course, unidb_lecturers AS lecturer, unidb_students AS student
WHERE student.mentor = course.rep_id AND lecturer.staff_no = course.coord_no;

-- Name the lecturers whose office is in G-block as well naming the students that are not from NZ
SELECT
  lecturer.fname,
  lecturer.lname
FROM unidb_lecturers lecturer
WHERE lecturer.office LIKE 'G%'
UNION
SELECT
  students.fname,
  students.lname
FROM unidb_students students
WHERE students.country != 'NZ';

-- List the course co-ordinator and student rep for COMP219
SELECT
  lecturer.fname,
  lecturer.lname,
  student.fname,
  student.lname
FROM unidb_courses AS course, unidb_lecturers AS lecturer, unidb_students AS student
WHERE lecturer.staff_no = course.coord_no AND
      student.id = course.rep_id AND
      course.num = 219 AND
      course.dept = 'comp';